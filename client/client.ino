#include <SPI.h>
#include <Time.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <EEPROM.h>
#include <avr/sleep.h>
#include <dht11.h>

// Hardware configuration
//


dht11 DHT11;
#define DHT11PIN 6
#define DHT11POWERPIN 5
//#define aref_voltage 3.3;


//-------------------------------------------------------------------------
// Set up nRF24L01 radio on SPI bus plus pins 9 & 10
//-------------------------------------------------------------------------

RF24 radio(9,10);

//Specify the reading pipes
const uint64_t readingPipes[5] = { 0xFFFFFF01 };
//const uint64_t readingPipes[1] = {0xF0F0F0F0D2LL};

//specify the writing pipes
const uint64_t writingPipes[1] = { 0xFFFFFF11 };
//const uint64_t writingPipes[1] = {0xF0F0F0F0E1LL};

unsigned long EEPROMStatus = 0; //Each bit in this number represent one space for long number to store in EEPROM. 1 means the space is occupied, 0 menas the space is free
unsigned long PACKET; // Main packet on which the operations are performed


byte packetProtokol = 0;
byte packetDestination = 0;
byte packetSource = 0;
byte packetDataType = 0;
int packetTime = 0;
byte packetData = 0;

byte tempPacketData = 0;

unsigned long lastRequestSecond1;
boolean noSleep = false; // Specify if the AVR will be put to sleep or not, used during watering to not sleeep
unsigned long waterUntilMillis;
byte waterCircuit1Pin = 8;
byte waterCircuit2Pin = 7;
byte currentWaterCircuitPin;


//-------------------------Arduino Sleep------------------- start
volatile int sleep_count = 1000; // Keep track of how many sleep. 1000 is here to let Arduino to send data from sensor first after restart and then go to sleep
const int interval = 1; // Interval in minutes between waking
const int sleep_total = (interval*20)/8; // Approximate number 


void goToSleep()   
{
  // The ATmega328 has five different sleep states.
  // See the ATmega 328 datasheet for more information.
  // SLEEP_MODE_IDLE -the least power savings 
  // SLEEP_MODE_ADC
  // SLEEP_MODE_PWR_SAVE
  // SLEEP_MODE_STANDBY
  // SLEEP_MODE_PWR_DOWN -the most power savings
  // I am using the deepest sleep mode from which a
  // watchdog timer interrupt can wake the ATMega328

  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // Set sleep mode.
  sleep_enable(); // Enable sleep mode.
  sleep_mode(); // Enter sleep mode.
  // After waking from watchdog interrupt the code continues
  // to execute from this point.

  sleep_disable(); // Disable sleep mode after waking.
}


void watchdogOn() {
  
// Clear the reset flag, the WDRF bit (bit 3) of MCUSR.
MCUSR = MCUSR & B11110111;
  
// Set the WDCE bit (bit 4) and the WDE bit (bit 3) 
// of WDTCSR. The WDCE bit must be set in order to 
// change WDE or the watchdog prescalers. Setting the 
// WDCE bit will allow updtaes to the prescalers and 
// WDE for 4 clock cycles then it will be reset by 
// hardware.
WDTCSR = WDTCSR | B00011000; 

// Set the watchdog timeout prescaler value to 1024 K 
// which will yeild a time-out interval of about 8.0 s.
WDTCSR = B00100001;

// Enable the watchdog timer interupt.
WDTCSR = WDTCSR | B01000000;
MCUSR = MCUSR & B11110111;

}

ISR(WDT_vect)
{
sleep_count ++; // keep track of how many sleep cycles have been completed.
}




void wateringGarden(byte duration, byte circuit){
  unsigned long realDuration = (unsigned long) duration * 3000L; //180000

  tempPacketData = packetData;

  
  switch (circuit) {
    case 1: currentWaterCircuitPin = waterCircuit1Pin; break;
    case 2: currentWaterCircuitPin = waterCircuit2Pin; break;
    default: currentWaterCircuitPin = 0; break;
  }
   
  Serial.print("Watering will last for ");
  Serial.print(realDuration); //180000
  Serial.println(" seconds.");
  
  Serial.print("Watering circuit ");
  Serial.println(circuit);
  digitalWrite(currentWaterCircuitPin,HIGH);
  noSleep = true;
  
  if (millis() + realDuration < millis()) {
    waterUntilMillis = 4294964295; // In case that AVR is on end with millis counting and will be counted again from 0 then it will watering just till end of unsigned long millis - 5 seconds.
  } else {
    waterUntilMillis = millis() + realDuration; // Duration can be 1-9, then it is trippled and multiple by miliseconds and then by 60 seconds to get minutes
    Serial.print("waterUntilMillis:");
    Serial.println(waterUntilMillis);
    Serial.print("millis:");
    Serial.println(millis());
  }
  
}


void sendWateringFinished() {
  packetProtokol = 0;
  packetDestination = 0;
  packetSource = 3;
  packetDataType = 4;
  packetTime = 1500;
  packetData = tempPacketData;

  implodePacket();
  proceedPacket();
  sendDataToDestination(0);
}



void sendDataToDestination (byte destination) {
  byte gatewayDestination;
  EEPROMStatus = readPacketEEPROM(32);
  //Serial.println(EEPROMStatus);
  //delay(3000);

  radio.stopListening();
  delay(20);
   
  switch (destination) {
    // Rulles where to send packet for specific client
    case 0: gatewayDestination = 0; break;
    case 1: gatewayDestination = 0; break;
    case 2: gatewayDestination = 0; break; //
    default: gatewayDestination = 0; break;
  }
  
  radio.openWritingPipe(writingPipes[destination]);

  for (byte i = 0; i<32; i++) {
    if (bitRead(EEPROMStatus,i) == 1) { // If data found in the EEPROM on specified position
      explodePacket(readPacketEEPROM(i));
      implodePacket();
      if (packetDestination==destination) { // If packet destination is RF24 main device connected to internet then it will start the data to be send to it
        //Serial.println(millis());
        bool ok = false;
        unsigned long retryIntervalMillisReached = millis()+ (unsigned long) 8100;
        while ((millis()<retryIntervalMillisReached) && (!ok)) {
          ok = radio.write( &PACKET, sizeof(unsigned long) );
        }
        //Serial.println(millis());
        if (ok) {
          printf("Data send successfully.");
          EEPROMStatusChange(i,0);
          
        } else {
          printf("Failed to send data.");
          if (packetData != 4) {
            EEPROMStatusChange(i,0); 
          }
        }       
        
      }
      delay(10);
      radio.startListening();
      delay(10);
      radio.stopListening();
    }
  }

 
  
  radio.startListening();
}


void explodePacket (unsigned long packet) {
//byte buf[4];
//byte temp;
unsigned long transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 25;
transmitDataTemp = transmitDataTemp >> 25;
packetData = transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 14; //16
transmitDataTemp = transmitDataTemp >> 21; //23
packetTime = transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 10;
transmitDataTemp = transmitDataTemp >> 28;
packetDataType = transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 6;
transmitDataTemp = transmitDataTemp >> 28;
packetSource = transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 2;
transmitDataTemp = transmitDataTemp >> 28;
packetDestination = transmitDataTemp;

transmitDataTemp = packet;
packetProtokol = transmitDataTemp >> 30;

}


void implodePacket () {
  PACKET = 0;
  PACKET = (unsigned long) ((unsigned long)packetProtokol << 30) | ((unsigned long)packetDestination << 26) | ((unsigned long)packetSource << 22) | ((unsigned long)packetDataType << 18) | ((unsigned long)packetTime << 7) | (unsigned long) packetData ;
   
}



void proceedPacket () {
  // If packetTime is 1500 it means the time of packet is not yet set and will be set now. Must be on first place here to add the time stamp before the packet is stored in EEPROM
  /*
  if (packetTime == 1500) {
    if (timeStatus () == timeSet ) {
      packetTime = (hour() * 60) + minute();
    }
  }
  */
  
  /*
  // If packetDestinatin is 2 then it means the destination is the main point on the garden
  
  if (packetDestination == 0) {
    implodePacket ();
    writePacketEEPROM (PACKET);
    sendDataToDestination(packetDestination);
    clearPACKET();
    
  }
  */
  
  // If packetDestinatin is 3 then it means the destination is just me - the client sensor (in this case number 3)
  if (packetDestination == 3) {
    // Now there will be procedures for the Garden main point if the packet belongs directly to it
    implodePacket ();
    switch (packetDataType) {

      // 1 - Time synchronization packet received
      case 1:
        Serial.println("Packet for time synchronization.");
        setTime (packetTime / 60, packetTime % 60, packetData, day(), month(), year());
      break;
      
      // 2 - Date synchronization packet received
      case 2:
        Serial.println("Packet for date synchronization.");
        setTime (hour(), minute(), second(), packetTime % 100, packetTime / 100, packetData + 2015);
      break;
      
      // 3 - Command packet received
      case 3:
        Serial.println("Command packet.");
        //Serial.println(packetData);
        switch (packetData) {
          // 1 - Packet which tells that client (packetSource) is listening
          case 0:
            sendDataToDestination(packetSource);
          break;          
        }
      break;

      // 4 - Watering command received
      case 4:
        Serial.println("Watering packet.");
        if (noSleep) { // Check if avr is set to noSleep, it means that watering is currently in progress. In this case it will only store the data in EEPROM
          Serial.println("Watering in progress, can not start new watering.");
          /*
          implodePacket ();
          writePacketEEPROM (PACKET);
          clearPACKET();
          */
        } else { // If no watering in progress then start watering imediately
        Serial.println(packetData / 10);
        Serial.println(packetData % 10);
        wateringGarden(packetData / 10, packetData % 10 );
        }        
      break;

      default:
        // kod
        Serial.println("Data type without defined process.");
    }


  } else { // If packetDestination is different then 2 then the packet will be stored in EEPROM
    implodePacket ();
    writePacketEEPROM (PACKET);
    sendDataToDestination(packetDestination);
    clearPACKET();
  }



}


void clearEEPROM () {
  for (byte i = 0; i<136; i++) {
    EEPROM.write(i, 0);
  }
}


void clearPACKET() {
  PACKET = 0; 
}

byte getFreeEEPROMPosition() {
  int aTime;
  int smallestTime = 1499;
  byte freePosition;
  unsigned long packet;
  
  EEPROMStatus = readPacketEEPROM(32);
  
  for (byte i = 0; i<32; i++) {
    if (bitRead(EEPROMStatus,i) == 0) {
      return i;
    }
  }
  // If no free position found then the oldest record will be replaced
  for (byte i = 0; i<32; i++) {
    packet = readPacketEEPROM(i*4); //read the packet from EEPROM
    packet = packet << 16;          //get the time value of the packet
    packet = packet >> 24;
    aTime = (int) packet;
    if (aTime < smallestTime) {    //find the smallest time of the packet
      smallestTime = aTime;
      freePosition = i;
    }
  }  
  return freePosition;
}


void EEPROMStatusChange(byte positionToChange, boolean valueToSet) {
  EEPROMStatus = readPacketEEPROM(32);
  bitWrite(EEPROMStatus,positionToChange, valueToSet);
  //Then also the EEPROMStatus value is written to position 128,129,130,131 in EEPROM (unsigned long)
  byte four = (EEPROMStatus);
  byte three = (EEPROMStatus >> 8);
  byte two = (EEPROMStatus >> 16);
  byte one = (EEPROMStatus >> 24);
  
  EEPROM.write(128, four);
  EEPROM.write(129, three);
  EEPROM.write(130, two);
  EEPROM.write(131, one);
  
  if (valueToSet == 0) { // If value to set is 0 it means that EEPROM on specific position also need to be cleared. Zero is written to EEPROM then.
    positionToChange = positionToChange * 4;
    EEPROM.write(positionToChange, 0);
    EEPROM.write(positionToChange + 1, 0);
    EEPROM.write(positionToChange + 2, 0);
    EEPROM.write(positionToChange + 3, 0);
  }
}


//This function will write a 4 byte (32bit) long to the eeprom at
//the specified address to adress + 3.
void writePacketEEPROM(unsigned long packet) {
  Serial.println("Now writing to EEPROM.");
  byte address = getFreeEEPROMPosition();
  address = address * 4;
  //Decomposition from a long to 4 bytes by using bitshift.
  //One = Most significant -> Four = Least significant byte
  byte four = (packet);
  byte three = (packet >> 8);
  byte two = (packet >> 16);
  byte one = (packet >> 24);

  //Write the 4 bytes into the eeprom memory.
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);

  EEPROMStatusChange(address/4, 1);
/*
  //When packet is written to EEPROM then bit on specific position in EEPROMStatus is also changed to notify that the address is occupied
  EEPROMStatus = readPacketEEPROM(32);
  bitWrite(EEPROMStatus,address / 4 ,1);
  //Then also the EEPROMStatus value is written to position 128,129,130,131 in EEPROM (unsigned long)
  four = (EEPROMStatus);
  three = (EEPROMStatus >> 8);
  two = (EEPROMStatus >> 16);
  one = (EEPROMStatus >> 24);
  
  EEPROM.write(128, four);
  EEPROM.write(129, three);
  EEPROM.write(130, two);
  EEPROM.write(131, one);
  */
}


unsigned long readPacketEEPROM(int address) {
  //Read the 4 bytes from the eeprom memory.
  address = address * 4;
  unsigned long four = EEPROM.read(address);
  unsigned long three = EEPROM.read(address + 1);
  unsigned long two = EEPROM.read(address + 2);
  unsigned long one = EEPROM.read(address + 3);

  //Return the recomposed long by using bitshift.
  return ((four << 0) | (three << 8) | (two << 16) | (one << 24));
}



void sendData(){
  implodePacket();
  proceedPacket();
  explodePacket(PACKET); 
}


void requestDataFromMainPointOnTheGarden(){
   
  packetProtokol = 0;
  packetDestination = 2;
  packetSource = 3;
  packetDataType = 3;
  packetTime = 0;
  packetData = 0;

  implodePacket();
  radio.stopListening();
  delay(20);
  
  radio.openWritingPipe(writingPipes[0]);

  bool ok = radio.write( &PACKET, sizeof(unsigned long) );
  if (ok) {
    printf("Data send successfully.");
  } else {
     printf("Failed to send data.");
  }

  radio.startListening(); 
}


void sendPacketsFromDHT11(){
  digitalWrite(5,HIGH);
  delay(1000); // to get stable reading
  int chk = DHT11.read(DHT11PIN);
  Serial.println((int)DHT11.humidity);
  Serial.println((int)DHT11.temperature);
  //Serial.println((int)dewPointFast(DHT11.temperature, DHT11.humidity));
  digitalWrite(5,LOW);
  
  // Prepare and send packet for temperature
  packetProtokol = 0;
  packetDestination = 0;
  packetSource = 3;
  packetDataType = 15;
  packetTime = 1500;
  packetData = (int)DHT11.temperature;
  sendData();
  
  // Prepare and send packet for humidity
  packetProtokol = 0;
  packetDestination = 0;
  packetSource = 3;
  packetDataType = 14;
  packetTime = 1500;
  packetData = (int)DHT11.humidity;
  sendData();
}


double dewPointFast(double celsius, double humidity)
{
	double a = 17.271;
	double b = 237.7;
	double temp = (a * celsius) / (b + celsius) + log(humidity*0.01);
	double Td = (b * temp) / (a - temp);
	return Td;
}



void setup() {
  // Remove when testing done
  clearEEPROM();
  // put your setup code here, to run once:
  // Start serial communication
  Serial.begin(9600);
  printf_begin(); // Is here to be able to print details of RF24. When not needed it can be commented
  // Read status of EEPROM to be able to determine on which positions the data are stored
  EEPROMStatus = readPacketEEPROM (128);
  // Set time and date to 00:00:00 1. 1. 2100. Otherwise the time is wrongly set if some real time would be not set here. Later on it will be set to correct date and time when packet with synchronization received.
  setTime(0,0,0,1,1,2100);
  
  // Setup the watchdog for AVR sleep
  watchdogOn();
 
  // Pin for DHT11 sensor to power it on
  pinMode(5, OUTPUT);
  //digitalWrite(5,HIGH);
  //delay(500);
 
  // Only for testing, coment it later
  pinMode(8,OUTPUT);
  
  
  EEPROMStatus = readPacketEEPROM (128); // Read the 33rd unsigned long record in EEPROM and assigne it to EEPROMStatus variable. It is here to be able to still work with values stored in EEPROM even the power was down for some time, to see on which addresses are stored packets
  //
  // Setup and configure rf radio
  //
  radio.begin();
  
  // Here is settings which should help to operate on long range distance
  // Max power 
  radio.setPALevel( RF24_PA_MAX ) ; 
 
// Min speed (for better range I presume)
  radio.setDataRate( RF24_250KBPS ) ;
  
  // 8 bits CRC
  radio.setCRCLength( RF24_CRC_8 ) ;

  // optionally, increase the delay between retries & # of retries
  radio.setRetries(15,15);

  // optionally, reduce the payload size.  seems to
  // improve reliability
  radio.setPayloadSize(32);


  radio.openWritingPipe(writingPipes[0]);
  radio.openReadingPipe(1,readingPipes[0]);
  
  radio.startListening();
  //Can be disabled - enabled for testing
  radio.printDetails();

delay (300);
}




void loop() {

if (sleep_count > sleep_total) {
  radio.powerUp();
  delay(100);
  Serial.println("Send packet to main point on the garden.");
  // Prepare packet with data to send
  
  sendPacketsFromDHT11();
     
  delay(100);
  
   
  Serial.println("Send packet to request posible data from main point on the garden.");
  // Prepare packet with data to send - command packet - to get possible commands stored on garden main point  packetProtokol = 0;
  requestDataFromMainPointOnTheGarden();
  
  // This part may be change as to be sure all data received correctly and not going sleep earlier
  delay(500); // Wait for possible data receiving
  
  if ( radio.available() )
    {
      // Dump the payloads until we've gotten everything
      bool done = false;
      while (!done)
      {
        // Fetch the payload, and see if this was the last one.
        done = radio.read( &PACKET, sizeof(unsigned long) );

        // Spew it
        printf("Got payload %lu...",PACKET);
        Serial.println(PACKET);
	// Delay just a little bit to let the other unit
	// make the transition to receiver
	delay(20);
      }

      // When all data received then use the protocolv1 to analyze the packet received
      explodePacket (PACKET);
      proceedPacket ();
      
    }
  
  sleep_count = 0; 
}
  

if ((millis()>waterUntilMillis) && noSleep) {
  noSleep = false;
  waterUntilMillis = 0;
  digitalWrite(currentWaterCircuitPin,LOW);
  sendWateringFinished();
}

if (!noSleep) { // it is here to avoid sleeping of avr at time of watering
  Serial.println("Go to sleep now.");
  radio.powerDown();
  delay(50);
  goToSleep();
  delay(50);
  Serial.println("Wake up now.");
}

}
