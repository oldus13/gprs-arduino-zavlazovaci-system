
/*
  SendTemp30mins_v2

This sketch connects to the website through a GSM shield.

Circuit:
* GSM shield attached to an Arduino
* SIM card with internet connection

*/

//------ GSM settings ------
// libraries (Use with modified library!)
#include <SPI.h>
#include <GSM.h>
#include <avr/wdt.h>
#include <Time.h>
#include <EEPROM.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <avr/sleep.h>
//#include "printf.h"


// APN data
#define GPRS_APN       "internet.t-mobile.cz" // your GPRS APN
#define GPRS_LOGIN     "gprs"    // your GPRS login
#define GPRS_PASSWORD  "gprs" // your GPRS password

char server[] = ""; // enter the web site in format xxxxxx.xxx for example adruinosite.net - It is the web site where data will be send to the PHP script
char path[] = ""; // enter the folder in which the script is located + file name and variable which will be posted in header "?data=". For example "/arduino/getdata.php?data="
int port = 80; // port 80 is the default for HTTP

//------ GSM settings END------

// Needed for getting time from GSM modul
#define MY_BUFFER_SIZE 20

// Number which specify after how many retries of GSM connection will be GSM turned down and Arduino will also reset itselve
#define GSMRETRIES 4
#define GPRSRETRIES 4

#define GSM_FAIL 1
#define GPRS_FAIL 2
#define NO_EEPROM_DATA 3
#define RESPONSE_FAIL 4
#define SERVER_FAIL 5
#define OK 6


#define GSM_MODUL_CLOCK_SOURCE 1
#define WEB_PACKET_CLOCK_SOURCE 2

// Web connection interval in seconds
#define WEBINTERVAL 300
unsigned long AVRsleepMillisStart;
time_t lastWebConnectedTime = 0;
time_t timeToStoreInEEPROM = 0;

// Interrupt variables
byte interruptID = 0;
byte interruptCounter = 0;
byte interruptBreak = 0;
byte lastInterruptID = 0;
int mainCodeLoopCounter = 0;

// Interrupt checkpoint IDs
#define I_GSM_GPRS_FAIL 1
#define I_GPRS_FAIL 2
#define I_WEB_CON_INTERVAL_REACHED 3
#define I_UNSPEC_LONG_WAIT 4 // Used for long wait interrupt counter, on places where usually disableWatchdog() function would be normally used.
#define I_GSM_MODEM_START 5
#define I_DATA_SENDING_TO_WEB 6
#define I_MAIN_LOOP_COUNTER 7
#define I_RADIO_AVAILABLE 8
#define I_TEST 9
#define I_INITIAL_SETUP 10

#define INTERRUPTLIGHTPIN 4

int a = 0;

// Variables for getting data from the internet
byte maxBuffSize = 100;
char clientLine[100];
int index = 0;

//AVR variables which are able to be modified via web interface
int AVRsleepDuration;
int AVRactiveDuration;
int GPRSconnectionInterval;

// Initialize the GSM library instance
GSMClient client;
GPRS gprs;
GSM gsmAccess; 

byte counterGSMRetries = 0;

// Settings of EEPROM
unsigned long EEPROMStatus = 0; //Each bit in this number represent one space for long number to store in EEPROM. 1 means the space is occupied, 0 menas the space is free
unsigned long PACKET; // Main packet on which the operations are performed

#define T_TIME_EEPROM_ADDRESS 400
#define INTERRUPT_EEPROM_ADDRESS 410
#define AVRsleepDuration_EEPROM_ADDRESS 414
#define AVRactiveDuration_EEPROM_ADDRESS 415
#define GPRSconnectionInterval_EEPROM_ADDRESS 416

// Hardware RF24 configuration
//
// Set up nRF24L01 radio on SPI bus plus pins 9 & 10
RF24 radio(9,10);

//Specify the reading pipes
const uint64_t readingPipes[5] = { 0xFFFFFF10, 0xFFFFFF11, 0xFFFFFF12, 0xFFFFFF13, 0xFFFFFF14 };
//const uint64_t readingPipes[1] = {0xF0F0F0F0D2LL};

//specify the writing pipes
const uint64_t writingPipes[5] = { 0xFFFFFF00, 0xFFFFFF01, 0xFFFFFF02, 0xFFFFFF03, 0xFFFFFF04 };
//const uint64_t writingPipes[1] = {0xF0F0F0F0E1LL};



byte packetProtokol = 0;
byte packetDestination = 0;
byte packetSource = 0;
byte packetDataType = 0;
int packetTime = 0;
byte packetData = 0;

byte tempPacketData = 0;

boolean timeAlreadySet = false;






void watchdogSetup(void)
{
  cli();       // disable all interrupts
  wdt_reset(); // reset the WDT timer
  /*
   WDTCSR configuration:
   WDIE = 1: Interrupt Enable
   WDE = 1 :Reset Enable
   WDP3 = 1 :For 8000ms Time-out
   WDP2 = 0 :For 8000ms Time-out
   WDP1 = 0 :For 8000ms Time-out
   WDP0 = 1 :For 8000ms Time-out
  */
  // Enter Watchdog Configuration mode:

  WDTCSR |= (1<<WDCE) | (1<<WDE);
  // Set Watchdog settings: (WDE 0 reset disabled
  WDTCSR = (1<<WDIE) | (0<<WDE) | (1<<WDP3) | (0<<WDP2) | (0<<WDP1) | (1<<WDP0);
  sei();  // re-enable interrupts

}


void disableWatchdog()
{
  cli();
  wdt_reset();
  MCUSR &= ~(1<<WDRF);
  WDTCSR |= (1<<WDCE) | (1<<WDE);
  WDTCSR = 0x00;
  sei();
  
}

ISR(WDT_vect) // Watchdog timer interrupt.
{
  if (interruptCounter >= interruptBreak) {
    digitalWrite(INTERRUPTLIGHTPIN,HIGH);
    writeTimeToEEPROM(timeToStoreInEEPROM);
    writeInterruptCheckpointToEEPROM(interruptID);
    MCUSR = 0;
    WDTCSR |= 0b00011000;    //WDCE y WDE = 1 --> config mode
    WDTCSR = 0b00001000 | 0b000100;    
  } else {
    interruptCounter++;
    wdt_reset();
  }
  
}



void goToSleep()   
{
  // The ATmega328 has five different sleep states.
  // See the ATmega 328 datasheet for more information.
  // SLEEP_MODE_IDLE -the least power savings 
  // SLEEP_MODE_ADC
  // SLEEP_MODE_PWR_SAVE
  // SLEEP_MODE_STANDBY
  // SLEEP_MODE_PWR_DOWN -the most power savings
  // I am using the deepest sleep mode from which a
  // watchdog timer interrupt can wake the ATMega328

  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // Set sleep mode.
  sleep_enable(); // Enable sleep mode.
  sleep_mode(); // Enter sleep mode.

  sleep_disable(); // Disable sleep mode after waking.
}


void sleepWatchdogOn() {
  
// Clear the reset flag, the WDRF bit (bit 3) of MCUSR.
MCUSR = MCUSR & B11110111;
  
// Set the WDCE bit (bit 4) and the WDE bit (bit 3) 
// of WDTCSR. The WDCE bit must be set in order to 
// change WDE or the watchdog prescalers. Setting the 
// WDCE bit will allow updtaes to the prescalers and 
// WDE for 4 clock cycles then it will be reset by 
// hardware.
WDTCSR = WDTCSR | B00011000; 

// Set the watchdog timeout prescaler value to 1024 K 
// which will yeild a time-out interval of about 8.0 s.
WDTCSR = B00100001;

// Enable the watchdog timer interupt.
WDTCSR = WDTCSR | B01000000;
MCUSR = MCUSR & B11110111;

}






void setupClock(byte clockSource){
  int timeout = 5000; // 5 seconds
  char myBuffer[MY_BUFFER_SIZE];
  int start;
  bool resp;
 
  if (clockSource == GSM_MODUL_CLOCK_SOURCE) { // If clock source is the GSM modul
    theGSM3ShieldV1ModemCore.theBuffer().flush();
    Serial.println(F("sending AT+QNITZ"));
    theGSM3ShieldV1ModemCore.genericCommand_rqc("AT+QNITZ=1");
    delay(5000);
    
    start = millis();
    Serial.println(F("sending AT+QLTS"));
    theGSM3ShieldV1ModemCore.genericCommand_rqc("AT+QLTS");
  
    myBuffer[0] = 0;
    while((millis() - start) < timeout) {
      theGSM3ShieldV1ModemCore.genericParse_rsp(resp, "QLTS", "OK");
      if(resp) {
        if(theGSM3ShieldV1ModemCore.theBuffer().extractSubstring("+QLTS: \"", "\"", myBuffer, MY_BUFFER_SIZE)) {
          break;
        }
      }
    }
  }
  
  if (clockSource == WEB_PACKET_CLOCK_SOURCE) {// If clock source is the packet from web
    byte i;
    for (i=0; i<17; i++) {
      myBuffer[i] = clientLine[i+10];
    }
    for (i=17; i<22; i++) {
      myBuffer[i] = '0';
    }
  }
  
  
  if(myBuffer[0]) {
    char buf2[1];
    Serial.print(F("Clock will be set to = "));
    Serial.println(myBuffer);
    // Set internal time of the Arduino
    // Year
    buf2[0] = myBuffer[0];
    buf2[1] = myBuffer[1];
    int intBufYear = 2000 + atoi(buf2);
    //Month
    buf2[0] = myBuffer[3];
    buf2[1] = myBuffer[4];
    int intBufMonth = atoi(buf2);
    //Day
    buf2[0] = myBuffer[6];
    buf2[1] = myBuffer[7];
    int intBufDay = atoi(buf2);
    //Hour
    int intBufZonePlus;
    if (clockSource == GSM_MODUL_CLOCK_SOURCE) {
      //char buf[2]; // to read the time zone which is the last 2 digits from GSM. And it say how many quarter of hours to add. For example 08 menas 2 hours, 04 means 1 hour.
      buf2[0] = myBuffer[18];
      buf2[1] = myBuffer[19];
      intBufZonePlus = atoi(buf2) / 4;
    } else {
      intBufZonePlus = 0;
    }
    buf2[0] = myBuffer[9];
    buf2[1] = myBuffer[10];
    int intBufHour = atoi(buf2) + intBufZonePlus;
    //Minute
    buf2[0] = myBuffer[12];
    buf2[1] = myBuffer[13];
    int intBufMinute = atoi(buf2);
    //Second
    buf2[0] = myBuffer[15];
    buf2[1] = myBuffer[16];
    int intBufSecond = atoi(buf2);

    setTime (intBufHour, intBufMinute, intBufSecond, intBufDay, intBufMonth, intBufYear);

    timeAlreadySet = true;
    delay(100);
    if (clockSource == WEB_PACKET_CLOCK_SOURCE) {
      Serial.println(F("Clock set from WEB to: "));
    }
    if (clockSource == GSM_MODUL_CLOCK_SOURCE) {
      Serial.println(F("Clock set from GSM to: "));
    }
    Serial.print(hour());
    Serial.print(":");
    Serial.print(minute());
    Serial.print(":");
    Serial.print(second());
    Serial.print(" - ");
    Serial.print(day());
    Serial.print("-");
    Serial.print(month());
    Serial.print("-");
    Serial.print(year());
    timeAlreadySet = true;
    
  } else {
    Serial.println(F("GSM clock = nothing"));
    if (now()< (time_t) 500000) {
      timeAlreadySet = false;
      
      packetProtokol = 0;
      packetDestination = 0;
      packetSource = 1;
      packetDataType = 3;
      packetTime = 0;
      packetData = 1;
      implodePacket();
      writePacketEEPROM(PACKET);
    }
  }
}



void explodePacket (unsigned long packet) {
//byte buf[4];
//byte temp;
unsigned long transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 25;
transmitDataTemp = transmitDataTemp >> 25;
packetData = transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 14; //16
transmitDataTemp = transmitDataTemp >> 21; //23
packetTime = transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 10;
transmitDataTemp = transmitDataTemp >> 28;
packetDataType = transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 6;
transmitDataTemp = transmitDataTemp >> 28;
packetSource = transmitDataTemp;

transmitDataTemp = packet;
transmitDataTemp = transmitDataTemp << 2;
transmitDataTemp = transmitDataTemp >> 28;
packetDestination = transmitDataTemp;

transmitDataTemp = packet;
packetProtokol = transmitDataTemp >> 30;
}



void implodePacket () {
  PACKET = 0;
  PACKET = (unsigned long) ((unsigned long)packetProtokol << 30) | ((unsigned long)packetDestination << 26) | ((unsigned long)packetSource << 22) | ((unsigned long)packetDataType << 18) | ((unsigned long)packetTime << 7) | (unsigned long) packetData ;
   
}


void packetFromWebReceived(byte type){
  char buf4[4];
  char buf3[3];
  char buf2[2];
  int intBuf;
        
  // Prepare packet
 packetProtokol = 0;
 packetSource = 0;

 
 if  (type == 0){ // packetDestination segment received
   clearPACKET();
   buf2[0] = clientLine[18];
   buf2[1] = clientLine[19];
   intBuf = atoi(buf2);
   packetDestination = intBuf;
   Serial.print(F("PacketDestination - received data:"));
   Serial.println(packetDestination);
 }
 
  if  (type == 1){ // packetType segment received
   buf2[0] = clientLine[15];
   buf2[1] = clientLine[16];
   intBuf = atoi(buf2);
   packetDataType = intBuf;
   Serial.print(F("PacketType - received data:"));
   Serial.println(packetDataType);
 }
 
  if  (type == 2){ // packetTime segment received
    buf4[0] = clientLine[11];
    buf4[1] = clientLine[12];
    buf4[2] = clientLine[13];
    buf4[3] = clientLine[14];
    intBuf = atoi(buf4);
    packetTime = intBuf;
    Serial.print(F("PacketTime - received data:"));
    Serial.println(packetTime);
  }
 
  if  (type == 3){ // packetData segment received
    buf3[0] = clientLine[11];
    buf3[1] = clientLine[12];
    buf3[2] = clientLine[13];
    buf3[3] = clientLine[14];
    intBuf = atoi(buf3);
    packetData = intBuf;
    Serial.print(F("PacketData - received data:"));
    Serial.println(packetData);
  }

 if  (type == 4){ // packetEND segment received
   implodePacket();
   writePacketEEPROM(PACKET);
   Serial.print(F("Data received from web stored in EEPROM now: "));
   Serial.println(PACKET);
   clearPACKET();
 }
  
}




void clearEEPROM () {
  for (byte i = 0; i<136; i++) {
    EEPROM.write(i, 0);
  }
}


void clearPACKET() {
  PACKET = 0; 
}

byte getFreeEEPROMPosition() {
  int aTime;
  int smallestTime = 1499;
  byte freePosition;
  unsigned long packet;
  
  EEPROMStatus = readPacketEEPROM(32);
  
  for (byte i = 0; i<32; i++) {
    if (bitRead(EEPROMStatus,i) == 0) {
      return i;
    }
  }
  // If no free position found then the oldest record will be replaced
  for (byte i = 0; i<32; i++) {
    packet = readPacketEEPROM(i*4); //read the packet from EEPROM
    packet = packet << 16;          //get the time value of the packet
    packet = packet >> 24;
    aTime = (int) packet;
    if (aTime < smallestTime) {    //find the smallest time of the packet
      smallestTime = aTime;
      freePosition = i;
    }
  }  
  return freePosition;
}


void EEPROMStatusChange(byte positionToChange, boolean valueToSet) {
  EEPROMStatus = readPacketEEPROM(32);
  bitWrite(EEPROMStatus,positionToChange, valueToSet);
  //Then also the EEPROMStatus value is written to position 128,129,130,131 in EEPROM (unsigned long)
  byte four = (EEPROMStatus);
  byte three = (EEPROMStatus >> 8);
  byte two = (EEPROMStatus >> 16);
  byte one = (EEPROMStatus >> 24);
  
  EEPROM.write(128, four);
  EEPROM.write(129, three);
  EEPROM.write(130, two);
  EEPROM.write(131, one);
  
  if (valueToSet == 0) { // If value to set is 0 it means that EEPROM on specific position also need to be cleared. Zero is written to EEPROM then.
    positionToChange = positionToChange * 4;
    EEPROM.write(positionToChange, 0);
    EEPROM.write(positionToChange + 1, 0);
    EEPROM.write(positionToChange + 2, 0);
    EEPROM.write(positionToChange + 3, 0);
  }
}


//This function will write a 4 byte (32bit) long to the eeprom at
//the specified address to adress + 3.
void writePacketEEPROM(unsigned long packet) {
  byte address = getFreeEEPROMPosition();
  address = address * 4;
  //Decomposition from a long to 4 bytes by using bitshift.
  //One = Most significant -> Four = Least significant byte
  byte four = (packet);
  byte three = (packet >> 8);
  byte two = (packet >> 16);
  byte one = (packet >> 24);

  //Write the 4 bytes into the eeprom memory.
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);

  EEPROMStatusChange(address/4, 1);
}


unsigned long readPacketEEPROM(int address) {
  //Read the 4 bytes from the eeprom memory.
  address = address * 4;
  unsigned long four = EEPROM.read(address);
  unsigned long three = EEPROM.read(address + 1);
  unsigned long two = EEPROM.read(address + 2);
  unsigned long one = EEPROM.read(address + 3);

  //Return the recomposed long by using bitshift.
  return ((four << 0) | (three << 8) | (two << 16) | (one << 24));
}




void proceedPacket () {
  // If packetTime is 1500 it means the time of packet is not yet set and will be set now. Must be on first place here to add the time stamp before the packet is stored in EEPROM
  if (packetTime == 1500) {
    if (timeAlreadySet) {
      packetTime = (hour() * 60) + minute();
    } else {
      Serial.println(F("Packet will be lost as no time set yet."));
      goto timeNotSetNoDataStored;
    }
  }
  
  // If packetDestinatin is 2 then it means the destination is just me (it means that destination is the client on which this code is running - main point on the garden)
  if (packetDestination == 2) {
    // Now there will be procedures for the Garden main point if the packet belongs directly to it
    implodePacket ();
    
    switch (packetDataType) {
      /*
      // 1 - Time synchronization packet received
      case 1:
        //kod
        
        Serial.println("Packet pro synchronizaci casu.");
        setTime (packetTime / 60, packetTime % 60, packetData, day(), month(), year());
        timeAlreadySet = true;
      break;
      // 2 - Date synchronization packet received
      case 2:
        Serial.println("Packet pro synchronizaci datumu.");
        setTime (hour(), minute(), second(), packetTime % 100, packetTime / 100, packetData + 2015);
      break;
      */
      // 3 - Command packet received
      case 3:
        Serial.println(F("Command packet."));
        //Serial.println(packetData);
        switch (packetData) {
          // 1 - Packet which tells that client (packetSource) is listening
          case 0:
            Serial.println(F("Now start sending data to listening client, as he told me to do so..."));
            Serial.println(packetDestination);
            sendDataToDestination(packetSource);
          break;          
        }
      break;

      
      case 5:
        Serial.println(F("Service packet. - SLEEP OR ACTIVE DURATION SETTINGS PACKET"));
        //Serial.println(packetData);
        if ((packetData > 1) && (packetData < 8)) {
          tempPacketData = packetData;
          EEPROM.write(AVRsleepDuration_EEPROM_ADDRESS,(byte) packetData);
          createAVRSleepSettingsFinished();
        }
        if ((packetData > 7) && (packetData < 14)) {
          tempPacketData = packetData;
          EEPROM.write(AVRactiveDuration_EEPROM_ADDRESS,(byte) packetData);
          createAVRActiveSettingsFinished();
        }
        refreshSettingsFromEEPROM();
      break;

      case 6:
        Serial.println(F("Service packet. - GPRS WEN CONNECTION INTERVAL RECEIVED,"));
        //Serial.println(packetData);
        tempPacketData = packetData;
        EEPROM.write(GPRSconnectionInterval_EEPROM_ADDRESS,(byte) packetData);
        refreshSettingsFromEEPROM();
        createGPRSconnectionIntervalSettingsFinished();
      break;
      

      default:
        // kod
        Serial.println(F("...."));
    }


  } else { // If packetDestination is different then 2 then the packet will be stored in EEPROM
    implodePacket ();
    writePacketEEPROM (PACKET);
    clearPACKET();
  }

timeNotSetNoDataStored:
clearPACKET();

}


void proceedPacketsForMe() {
  EEPROMStatus = readPacketEEPROM(32);
  for (byte i = 0; i<32; i++) {
    if (bitRead(EEPROMStatus,i) == 1) { // If data found in the EEPROM on specified position
      explodePacket(readPacketEEPROM(i));
      implodePacket();
      if (packetDestination == 2){
        proceedPacket();
        EEPROMStatusChange(i,0);        
      }
    }
  }
}

void sendDataToDestination (byte destination) {
  byte gatewayDestination;
  EEPROMStatus = readPacketEEPROM(32);
  //Serial.println(EEPROMStatus);
  //delay(3000);

  radio.stopListening();
  delay(20);
   
  switch (destination) {
    // Rulles where to send packet for specific client
    case 0: gatewayDestination = 0; break; //
    case 1: gatewayDestination = 0; break; //
    case 3: gatewayDestination = 1; break; //
    case 4: gatewayDestination = 2; break; //
    case 5: gatewayDestination = 3; break; //
    case 6: gatewayDestination = 4; break; //
    default: gatewayDestination = 0; break;
  }
  
  radio.openWritingPipe(writingPipes[gatewayDestination]);

  for (byte i = 0; i<32; i++) {
    if (bitRead(EEPROMStatus,i) == 1) { // If data found in the EEPROM on specified position
      explodePacket(readPacketEEPROM(i));
      implodePacket();
      
      if (packetDestination == destination ) { // If packet destination is RF24 main device connected to internet then it will start the data to be send to it
        bool ok = radio.write( &PACKET, sizeof(unsigned long) );
        if (ok) {
          Serial.println(F("Data send successfully."));
          EEPROMStatusChange(i,0);
          
        } else {
          Serial.println(F("Failed to send data to client - watering!!!!!!."));
          Serial.print(F("Packet Data:"));
          Serial.println(packetDestination);
          Serial.println(packetSource);
          Serial.println(packetDataType);
          Serial.println(packetData);
          if (packetDataType == 1 || packetDataType == 2) { // If packet is packet for time synchronization then remove it from EEPROM anyway because it would become obsolete
            EEPROMStatusChange(i,0);
          
          }
        }

      }
      delay(10);
      radio.startListening();
      delay(10);
      radio.stopListening();
    }
  }

 
  
  radio.startListening();
  
}




void writeTimeToEEPROM(unsigned long timeToWrite) {
  int address = T_TIME_EEPROM_ADDRESS;
  //Decomposition from a long to 4 bytes by using bitshift.
  //One = Most significant -> Four = Least significant byte
  byte four = (timeToWrite);
  byte three = (timeToWrite >> 8);
  byte two = (timeToWrite >> 16);
  byte one = (timeToWrite >> 24);

  //Write the 4 bytes into the eeprom memory.
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);
}


void iCheckPoint (byte lastCycle, byte id) {
  wdt_reset();
  timeToStoreInEEPROM = now() + (time_t)(lastCycle * (byte)8) ;
  interruptCounter = 0;
  //lastInterruptID = interruptID;
  interruptID = id;
  interruptBreak = lastCycle;
}

void writeInterruptCheckpointToEEPROM(byte id) {
  EEPROM.write(INTERRUPT_EEPROM_ADDRESS,id);
}

byte readInterruptCheckpointFromEEPROM() {
  return EEPROM.read(INTERRUPT_EEPROM_ADDRESS);
}

bool refreshSettingsFromEEPROM() {
  byte tempAVRsleepDuration =  EEPROM.read(AVRsleepDuration_EEPROM_ADDRESS);
  byte tempAVRactiveDuration = EEPROM.read(AVRactiveDuration_EEPROM_ADDRESS);
  byte tempGPRSconnectionInterval = EEPROM.read(GPRSconnectionInterval_EEPROM_ADDRESS);
  if (tempAVRsleepDuration == 2) AVRsleepDuration = 250;
  if (tempAVRsleepDuration == 3) AVRsleepDuration = 500;
  if (tempAVRsleepDuration == 4) AVRsleepDuration = 1000;
  if (tempAVRsleepDuration == 5) AVRsleepDuration = 2000;
  if (tempAVRsleepDuration == 6) AVRsleepDuration = 4000;
  if (tempAVRsleepDuration == 7) AVRsleepDuration = 8000;
  if (tempAVRsleepDuration == 0) AVRsleepDuration = 8000;

  if (tempAVRactiveDuration == 8) AVRactiveDuration = 100;
  if (tempAVRactiveDuration == 9) AVRactiveDuration = 250;
  if (tempAVRactiveDuration == 10) AVRactiveDuration = 500;
  if (tempAVRactiveDuration == 11) AVRactiveDuration = 1000;
  if (tempAVRactiveDuration == 12) AVRactiveDuration = 3000;
  if (tempAVRactiveDuration == 13) AVRactiveDuration = 8000;
  if (tempAVRactiveDuration == 0) AVRactiveDuration = 250;

  if (tempGPRSconnectionInterval == 0) {
    GPRSconnectionInterval = WEBINTERVAL;
  } else {
    GPRSconnectionInterval = int (tempGPRSconnectionInterval) * (int) 60; // musi se zmenit na  "GPRSconnectionInterval = int (tempGPRSconnectionInterval) * (int) 10 * (int) 60;" 
  }
  Serial.print("AVRsleepDuration: ");
  Serial.println(AVRsleepDuration);
  Serial.print("AVRactiveDuration: ");
  Serial.println(AVRactiveDuration);
  Serial.print("GPRSconnectionInterval: ");
  Serial.println(GPRSconnectionInterval);
}


void createAVRSleepSettingsFinished() {
  packetProtokol = 0;
  packetDestination = 0;
  packetSource = 2;
  packetDataType = 5;
  packetTime = 1500;
  packetData = tempPacketData;

  implodePacket();
  proceedPacket();
}

void createAVRActiveSettingsFinished() {
  packetProtokol = 0;
  packetDestination = 0;
  packetSource = 2;
  packetDataType = 5;
  packetTime = 1500;
  packetData = tempPacketData;

  implodePacket();
  proceedPacket();
}


void createGPRSconnectionIntervalSettingsFinished() {
  packetProtokol = 0;
  packetDestination = 0;
  packetSource = 2;
  packetDataType = 6;
  packetTime = 1500;
  packetData = tempPacketData;

  implodePacket();
  proceedPacket();
}


void setup()
{
  // Disable watch dog
  disableWatchdog();
  watchdogSetup();
  pinMode(INTERRUPTLIGHTPIN,OUTPUT);
  iCheckPoint(10,I_INITIAL_SETUP);
  // initialize serial communications
  Serial.begin(9600);
  //printf_begin(); // Is here to be able to print details of RF24. When not needed it can be commented

  Serial.print(F("Interrupt occure because of this interrupt ID: "));
  Serial.println(readInterruptCheckpointFromEEPROM());
  writeInterruptCheckpointToEEPROM(0);

  refreshSettingsFromEEPROM();
  
   
  //Set time from EEPROM
  setTime(readPacketEEPROM(100));
  delay(100);
  if (now() < (time_t) 500000) {
    Serial.println(F("Time in EEPROM was less then 500000."));
    timeAlreadySet = false;
  } else {
    timeAlreadySet = true;
  }
  
  Serial.println(now());
  writeTimeToEEPROM(0);
    Serial.println(F("Time red from EEPROM: "));
    Serial.print(hour());
    Serial.print(":");
    Serial.print(minute());
    Serial.print(":");
    Serial.print(second());
    Serial.print(" - ");
    Serial.print(day());
    Serial.print("-");
    Serial.print(month());
    Serial.print("-");
    Serial.print(year());

  
  
  // Read status of EEPROM to be able to determine on which positions the data are stored
  EEPROMStatus = readPacketEEPROM (128);
  //setTime(0,0,0,1,1,2100);

  // Test packet for getting the date and time from website
  /*
  packetProtokol = 0;
  packetDestination = 0;
  packetSource = 2;
  packetDataType = 3;
  packetTime = 0;
  packetData = 1;
  */


  byte uploadResult;

  iCheckPoint(100,I_UNSPEC_LONG_WAIT);
  retriesOFGSMConnection:
  
  uploadResult = uploadReading();
  
  Serial.print(F("uploadResult: "));
  Serial.println(uploadResult);
  
  // wait for half an hour
  /*
  if (uploadResult == OK)
  {
    Serial.println(F("30 seconds delay.")); // puvodne tu bylo 30 minut
    delay(30000); // 30 minutes
    counterGSMRetries = 0;
  }
  */
  if (uploadResult == NO_EEPROM_DATA) {
    Serial.println(F("No data in EEPROM."));
    //counterGSMRetries = 0;
  }
  
  if (uploadResult == GSM_FAIL || uploadResult == GPRS_FAIL) {
    Serial.println(F("GSM_FAIL or GPRS_FAIL")); 
    delay(5000); // change to retry in 10 minutes
    counterGSMRetries++;
    if (counterGSMRetries >= GSMRETRIES) {
      //Serial.println("GSM going down. Arduino will restart itselve after.");
      gsmAccess.shutdown();
      delay(5000);
      //writeTimeToEEPROM(now());
      //watchdogSetup();
      iCheckPoint(1,I_GSM_GPRS_FAIL);
      delay(250);
 
    }
    goto retriesOFGSMConnection;
  }


  skipWebConnection: //Only for testing purposes

  proceedPacketsForMe();

  radio.begin();
  
  // Settings which should enable long distance communication
  // Max power 
  radio.setPALevel( RF24_PA_MAX ) ; 
 
  // Min speed (for better range I presume)
  radio.setDataRate( RF24_250KBPS ) ;
  
  // 8 bits CRC
  radio.setCRCLength( RF24_CRC_8 ) ;

  // optionally, increase the delay between retries & # of retries
  radio.setRetries(15,15);

  // optionally, reduce the payload size.  seems to
  // improve reliability
  radio.setPayloadSize(32);


  radio.openWritingPipe(writingPipes[0]);
  radio.openReadingPipe(1,readingPipes[0]);
  radio.openReadingPipe(2,readingPipes[1]);
  radio.openReadingPipe(3,readingPipes[2]);
  radio.openReadingPipe(4,readingPipes[3]);
  radio.openReadingPipe(5,readingPipes[4]);

  radio.startListening();
  //Can be disabled - enabled for testing
  radio.printDetails();

  AVRsleepMillisStart = millis() + AVRsleepDuration;
}






void loop()
{
  /*
  mainCodeLoopCounter++;
  if (mainCodeLoopCounter > 1000) {
    mainCodeLoopCounter = 0;
    iCheckPoint(2,I_MAIN_LOOP_COUNTER);
  }
  */

  if (millis() > AVRsleepMillisStart) {
    Serial.println(F("Going to sleep now."));
    delay(50);
    radio.powerDown();
    disableWatchdog();
    sleepWatchdogOn();
    goToSleep();
    Serial.println(F("Wake Up NOW."));
    AVRsleepMillisStart = millis() + AVRactiveDuration;
    adjustTime(8);
    radio.powerUp();
    watchdogSetup();
  }

  
  // Check if connection to internet needed
  if ( now() > lastWebConnectedTime + GPRSconnectionInterval) {
    Serial.println(interruptCounter);
    Serial.println(interruptBreak);
    Serial.println(interruptID);
    
    Serial.println(F("Arduino going to reboot and then connect to internet. Web connection interval reached."));
   
    iCheckPoint(0,I_WEB_CON_INTERVAL_REACHED);
    delay(10000);
    /*
    writeTimeToEEPROM(now());
    Serial.println(now());
    delay(100);
    disableWatchdog();
    watchdogSetup();
    delay(11000);
    */
  }
  
  
  if ( radio.available() )
    {
    iCheckPoint(4,I_RADIO_AVAILABLE);
    // Dump the payloads until we've gotten everything
    bool done = false;
    while (!done)
    {
      // Fetch the payload, and see if this was the last one.
      done = radio.read( &PACKET, sizeof(unsigned long) );

      // Spew it
      //printf("Got payload %lu...",PACKET);
      Serial.println(PACKET);
      // Delay just a little bit to let the other unit
      // make the transition to receiver
      delay(20);
    }

    // When all dota received then use the protocolv1 to analyze the packet received
    explodePacket (PACKET);
    proceedPacket ();  

    AVRsleepMillisStart = millis() + AVRactiveDuration;
    }
 
}

byte uploadReading()
{
  byte uploadReturnStatus;

  // Read 4 bytes from EEPROM starting from position 128, each bit specify if there is any packet stored on such position in EEPROM
  EEPROMStatus = readPacketEEPROM(32);
  boolean testIfDataForWebInEEPROM = false;
  Serial.print(F("EEPROMStatus:"));
  Serial.println(EEPROMStatus);
  for (byte i = 0; i < 32; i++) {
    if (bitRead(EEPROMStatus,i) == 1) {
      explodePacket (readPacketEEPROM(i));
      if (packetDestination==0) {
        testIfDataForWebInEEPROM = true;
      }
    }
  }

    
  // connection state
  // Start the modem with GSM.begin()
  iCheckPoint(5,I_GSM_MODEM_START);
  
  Serial.println(F("Connecting GSM."));
  //watchdogSetup();
  if(gsmAccess.begin()== GSM_READY) 
  {
    //wdt_reset();
    //disableWatchdog();
    delay(100);
    
    Serial.println(F("GSM connected."));
    //theGSM3ShieldV1ModemCore.println("AT+QTONEDET=1");
    delay(3000);
    setupClock(GSM_MODUL_CLOCK_SOURCE);
    //Serial.println(theGSM3ShieldV1ModemCore.println("AT+CCLK?"));
  }
  else
  {
    //wdt_reset();
    //disableWatchdog();
    delay(100);
    Serial.println(F("GSM connection failed."));
    Serial.println(F("Terminating GSM"));
    gsmAccess.shutdown();
    uploadReturnStatus = GSM_FAIL;
    return uploadReturnStatus;
  }

  //wdt_reset();
  //disableWatchdog();
  //delay(100);

  // MARK1
  Serial.println(F("Now checkind if any data in EEPROM to be sent to internet."));
  if (testIfDataForWebInEEPROM) { // perform sending to web only in case that there are some data stored in EEPROM
    Serial.println(F("Data found in EEPROM."));
  } else {
    Serial.println("No data in EEPROM.");
    packetProtokol = 0;
    packetDestination = 0;
    packetSource = 2;
    packetDataType = 5;
    packetTime = 1500;
    packetData = 1;
    implodePacket();
    proceedPacket();
    //gsmAccess.shutdown();
    //uploadReturnStatus = NO_EEPROM_DATA;
    //lastWebConnectedTime = now() - 2;
    //return uploadReturnStatus;
  }
     // attach the shield to the GPRS network with the APN, login and password
    Serial.println("Connecting GPRS.");
    // Retry 9 times before giving up
    unsigned loopCntGPRS = GPRSRETRIES;

    /*
    writeTimeToEEPROM(now());
    watchdogSetup();
    */
    
    while(loopCntGPRS--)
    {
      iCheckPoint(2,I_GPRS_FAIL);
      if(gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD)==GPRS_READY)
      {
        //wdt_reset();
        iCheckPoint(100,I_UNSPEC_LONG_WAIT);
        Serial.println(F("GPRS connected."));
        break;
      }
      else
      {
        //disableWatchdog();
        iCheckPoint(100,I_UNSPEC_LONG_WAIT);
        Serial.println(F("GPRS not connected"));
        delay(1000);
      }
      if(!loopCntGPRS)
      {
        Serial.println(F("GPRS connection failed."));
        Serial.println(F("Terminating GSM"));
      
        gsmAccess.shutdown();
        uploadReturnStatus = GPRS_FAIL;
        return uploadReturnStatus;
      }
      /*
      writeTimeToEEPROM(now());
      watchdogSetup();   
      */
    }

    iCheckPoint(7,I_DATA_SENDING_TO_WEB);
    // Connect to the server and send readings
    if (client.connect(server, port))
    {
      Serial.println(F("Connected to server."));
      // Make a HTTP request:
      client.print("GET ");
      client.print(path);
      boolean testStar = false;
      for (byte i = 0; i < 32; i++) {
        if (bitRead(EEPROMStatus,i) == 1) {
          explodePacket (readPacketEEPROM(i));
          if (packetDestination==0) {
            if (testStar) {
              client.print("*");
            } else {
              testStar = true;
            }
              
            client.print(packetSource);
            client.print("*");
            client.print(packetDataType);
            client.print("*");
            client.print(year());
            client.print("-");
            client.print(month());
            client.print("-");
            client.print(day());
            client.print("%20");
            client.print(packetTime / 60);
            client.print(":");
            client.print(packetTime % 60);
            client.print(":");
            client.print("00");
            client.print("*");
            client.print(packetData);
            //client.print("*");
            Serial.println(packetSource);
            Serial.println(packetDataType);
            Serial.println(year());
            Serial.println(month());
            Serial.println(day());
            Serial.println(packetTime / 60);
            Serial.println(packetTime % 60);
            Serial.println(packetData);


            
            //Serial.println(packetSource && "*" && packetDataType && "*" && packetData && "*" && year() && "-" && month() && "-" && day() && " " && packetTime / 60 && ":" && packetTime % 60 && ":" && "00" && "*");
            EEPROMStatusChange(i,0);
            PACKET = 0;
          }
        }
     
      }
      client.println(" HTTP/1.1");
      client.print("Host: ");
      client.println(server);
      client.println("Authorization: Basic bWFyaGluOaF1bm8xMjM0"); // Here you have to change the string after the Basic to fit your username and password for your website
      client.println();
      //lastWebConnectedTime = now() - 2;
      //delay(1000);
    } 
    else
    {
      Serial.println(F("Server connection failed")); //"Server connection failed"
      Serial.println(F("Terminating GSM"));
      gsmAccess.shutdown();
      uploadReturnStatus = SERVER_FAIL;
      iCheckPoint(100,I_UNSPEC_LONG_WAIT);
      return uploadReturnStatus;
    }

    // Read incoming server response
    //String outputString = "";
    //byte maxBuffSize = 30;
    //char clientLine[30];
    //byte a = 1;
    //int index = 0;
    while (client.available() || client.connected())
    {
      // if there are incoming bytes available from the server, read them
      if (client.available())
      {
        char c = client.read();

        /*
        outputString = outputString + c;
        if (outputString.length() > 15)
        {
          // stop the client.
          Serial.println("Disconnecting from server.");
          client.flush();
          client.stop();
          break;
        }
        */

        if (c != '\n' && c != '\r') {
          clientLine[index] = c;
          index++;
          // Are we too big for the buffer? Start tossing out data
          if (index >= maxBuffSize) {
             index = maxBuffSize - 1;
          }
    
        } else {
          // Got a \n or \r new line, which means the string is done.
          clientLine[index] = 0;
          index = 0;
          // Print it out for debugging.
          Serial.println(clientLine);

          if(strstr(clientLine, "DateTime") != 0) { //If date time received from PHP server
            //dateTimeSynchro();
            setupClock(WEB_PACKET_CLOCK_SOURCE);
          }
          if(strstr(clientLine, "packetDestination=") != 0) { //If date time received from PHP server
            packetFromWebReceived(0);
          }
          if(strstr(clientLine, "packetDataType=") != 0) { //If date time received from PHP server
            packetFromWebReceived(1);
          }
          if(strstr(clientLine, "packetTime=") != 0) { //If date time received from PHP server
            packetFromWebReceived(2);
          }
          if(strstr(clientLine, "packetData=") != 0) { //If date time received from PHP server
            packetFromWebReceived(3);
          }
          if(strstr(clientLine, "ENDPACKETDATA.") != 0) { //If date time received from PHP server
            packetFromWebReceived(4);
          }
          
        }
      }
    }
    //Serial.println("Server response;");
    //Serial.println(outputString);
    
    Serial.println(F("Terminating GSM"));
    gsmAccess.shutdown();
    lastWebConnectedTime = now() - 2;
    delay(1000);
    
    // check server response for 200 OK
    /*
    if (outputString.startsWith("HTTP/1.1 200 OK"))
    {
      uploadReturnStatus = OK;
      return uploadReturnStatus;
    }
    */
    iCheckPoint(100,I_UNSPEC_LONG_WAIT);
    uploadReturnStatus = RESPONSE_FAIL;
    return uploadReturnStatus;

  // MARK1
  
  
}



