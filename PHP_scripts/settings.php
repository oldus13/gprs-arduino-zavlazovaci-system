<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


include_once ("../php/variables.php");
include_once ("../arduino/functions.php");

$conn = mysqli_connect($myHost, $myUser, $myPassword,$myDb);
//$value1 = $_GET["data"];

//$pole=explode("*",$value1);


//var_dump ($pole);


if(! $conn )
{
  die('Could not connect: ' . mysqli_error());
}


$sql = "SELECT clientID, settingsID, value FROM settings ORDER BY clientID ASC";
$retval = mysqli_query( $conn, $sql);
if(! $retval ) {
  die('Could not retrive data: ' . mysqli_error($conn));
}


$sql2 = "SELECT packetData FROM waitingCommands WHERE ((status < 2) AND (packetDestination = 2) AND (packetDataType = 5) AND (packetData > 7) AND (packetData < 14)) LIMIT 1";
$retval2 = mysqli_query( $conn, $sql2);
if(! $retval ) {
  die('Could not retrive data: ' . mysqli_error($conn));
}

$sql3 = "SELECT packetData FROM waitingCommands WHERE ((status < 2) AND (packetDestination = 2) AND (packetDataType = 5) AND (packetData > 1) AND (packetData < 8)) LIMIT 1";
$retval3 = mysqli_query( $conn, $sql3);
if(! $retval ) {
  die('Could not retrive data: ' . mysqli_error($conn));
}

$sql4 = "SELECT packetData FROM waitingCommands WHERE ((status < 2) AND (packetDestination = 2) AND (packetDataType = 6)) LIMIT 1";
$retval4 = mysqli_query( $conn, $sql4);
if(! $retval ) {
  die('Could not retrive data: ' . mysqli_error($conn));
}


while($row = mysqli_fetch_array($retval, MYSQLI_ASSOC)) {
  if ($row["clientID"] == 2 && $row["settingsID"] == 1) { // GPRS settings 
    $valueToDisplay1 = $row["value"];
  }

  if ($row["clientID"] == 2 && $row["settingsID"] == 2) { // NRF active settings 
    $valueToDisplay2 = $row["value"] - 7;
  }

  if ($row["clientID"] == 2 && $row["settingsID"] == 3) { // NRF sleep settings 
    $valueToDisplay3 = $row["value"] - 1;
  }
}


$row2 = mysqli_fetch_array($retval2, MYSQLI_ASSOC);

$row3 = mysqli_fetch_array($retval3, MYSQLI_ASSOC);

$row4 = mysqli_fetch_array($retval4, MYSQLI_ASSOC);


?>

<div class="formular">
  <h2><span>Settings</span></h2>
  <form action="getdata.php?data=2*SET*6" method="post">
    <?php
    if (is_null($row4)) { ?>
      GPRS connection interval (1 to 250, interval is 10 minutes): <input type="text" name="settings1" size="2" value="<?php echo $valueToDisplay1; ?>">
      <?php
    } else { ?>
      GPRS connection interval (1 to 250, interval is 10 minutes): <input type="text" name="settings1" size="2" value="<?php echo $valueToDisplay1; ?>" disabled><B> Currently waiting value to be set: <?php echo $row4["packetData"]; ?></B>
    <?php } ?>

    <BR>


    <?php
    if (is_null($row2)) { ?>
      NRF active duration (1 = 100ms, 2 = 250ms, 3 = 500ms, 4 = 1s, 5 = 3s, 6 = 8s): <input type="text" name="settings2" size="4" value="<?php echo $valueToDisplay2; ?>">
      <?php
    } else { ?>
      NRF active duration (1 = 100ms, 2 = 250ms, 3 = 500ms, 4 = 1s, 5 = 3s, 6 = 8s): <input type="text" name="settings2" size="4" value="<?php echo $valueToDisplay2; ?>" disabled><B> Currently waiting value to be set: <?php echo $row2["packetData"] - 7; ?></B>
    <?php } ?>

    <BR>


    <?php
    if (is_null($row3)) { ?>
      NRF sleep duration (1 = 250ms, 2 = 500ms, 3 = 1s, 4 = 2s, 5 = 4s, 6 = 8s): <input type="text" name="settings3" size="4" value="<?php echo $valueToDisplay3; ?>">
      <?php
    } else { ?>
      NRF sleep duration (1 = 250ms, 2 = 500ms, 3 = 1s, 4 = 2s, 5 = 4s, 6 = 8s): <input type="text" name="settings3" size="4" value="<?php echo $valueToDisplay3; ?>" disabled><B> Currently waiting value to be set: <?php echo $row3["packetData"] - 1; ?></B>
    <?php } ?>

    <BR><BR>
    <input type="submit" value="Apply">

  <form>
</div>