SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `settings` (
  `clientID` tinyint(3) unsigned NOT NULL,
  `settingsID` tinyint(3) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `info` text COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `settings` (`clientID`, `settingsID`, `value`, `timeStamp`, `info`) VALUES
(2, 1, 13, '2015-10-13 14:54:21', 'Main GPRS point - GPRS connection interval (value 1 - 250)'),
(2, 2, 9, '2015-09-30 21:04:46', 'Main point - active duration (value 8 - 13)'),
(2, 3, 7, '2015-09-30 21:04:46', 'Main point - sleep duration (value 2 - 7)');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
